package ltos.timewarpeditor.scan.filterpro.singleton

import android.view.MotionEvent
import android.view.View
import ltos.timewarpeditor.scan.filterpro.iinterface.InvokeActivityMethod
import ltos.timewarpeditor.scan.filterpro.sealed.Capturemode

class MyTouchListener: View.OnTouchListener {
    var isVerical = false
    private var initX = 0F
    private var initY = 0F
    var listener:InvokeActivityMethod? = null
    var capturemode: Capturemode? = null

    override fun onTouch(view: View?, event: MotionEvent?): Boolean {

        when(event?.action)
        {
            MotionEvent.ACTION_DOWN->{
                initX = event.rawX
                initY = event.rawY
            }
            MotionEvent.ACTION_MOVE->{}
            MotionEvent.ACTION_UP->{

                if(capturemode == Capturemode.running)
                {
                    return false
                }
                if((event.rawX - initX)>=LocalSingelton.SWIPE_LENGTH)
                {
                    isVerical = false
                    listener?.invokeStartImageCapture()
                }
                else if((event.rawY - initY)>=LocalSingelton.SWIPE_LENGTH)
                {
                    isVerical = true
                    listener?.invokeStartImageCapture()
                }
            }
        }
        return true
    }


}