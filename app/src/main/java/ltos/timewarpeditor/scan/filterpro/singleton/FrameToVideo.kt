package ltos.timewarpeditor.scan.filterpro.singleton

import android.graphics.Bitmap
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.util.Log
import android.view.Surface

class FrameToVideo {

    private var mediaRecorder:MediaRecorder? = null
    private var surface: Surface? = null
    private var dWidth = 0
    private var dHeight = 0

    suspend fun setRecorder(outPutFilePath: String, width: Int?, height: Int?)
    {
        try {
            dWidth = width!!
            dHeight = height!!

            if(dWidth %2 !=0)
            {
                dWidth = dWidth+1
            }
            if(dHeight %2 !=0)
            {
                dHeight = dHeight+1
            }
            mediaRecorder = MediaRecorder()
            mediaRecorder?.setVideoSource(MediaRecorder.VideoSource.SURFACE)

            val camprofile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH)

            mediaRecorder?.setOutputFormat(camprofile?.fileFormat!!)
            mediaRecorder?.setVideoSize(dWidth, dHeight)
            mediaRecorder?.setVideoEncoder(camprofile?.videoCodec!!)
            mediaRecorder?.setVideoEncodingBitRate(camprofile?.videoBitRate!!)
            mediaRecorder?.setVideoFrameRate(30)

            mediaRecorder?.setOutputFile(outPutFilePath)
            mediaRecorder?.prepare()
            surface = mediaRecorder?.surface
            mediaRecorder?.start()
        } catch (e: Exception) {
            Log.e("TAG", "startMediaRecorder: ${e.message}", )
        }
    }

    suspend fun addFrame(bitmap: Bitmap)
    {
        try {
            val canvas = surface?.lockCanvas(null)
            bitmap.let{ canvas?.drawBitmap(it, 0F, 0F, null) }
            surface?.unlockCanvasAndPost(canvas)
            bitmap.recycle()
        } catch (e: Exception) {
            Log.e("TAG", "addFrame: ${e.message}", )
        }
    }

    suspend fun clearRecorder()
    {
        mediaRecorder?.stop();
        mediaRecorder?.reset();   // You can reuse the object by going back to setAudioSource() step
        mediaRecorder?.release()
    }


}