package ltos.timewarpeditor.scan.filterpro.activity

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.api.RetrofitClient
import ltos.timewarpeditor.scan.filterpro.databinding.ActivityShareBinding
import ltos.timewarpeditor.scan.filterpro.databinding.NativeadviewLayoutBinding
import ltos.timewarpeditor.scan.filterpro.databinding.QurekaNativeAdsBinding
import ltos.timewarpeditor.scan.filterpro.dataclass.Model_Btn
import ltos.timewarpeditor.scan.filterpro.singleton.LocalSingelton
import ltos.timewarpeditor.scan.filterpro.utiltiy.MyApplication
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShareActivity : AppCompatActivity() {

    private var binding:ActivityShareBinding? = null
    private var sharetype = ""

    private var nativeAd: NativeAd? = null
    private var qurekaimage = ""
    private var querekatext = ""
    private var url = ""
    private var checkqureka = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShareBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        setNativeAd()

        val from  = intent?.getStringExtra("from")
        var currentFilePath = ""
        when(from)
        {
            "image"-> {
                currentFilePath = LocalSingelton.finalBitmapPath
                sharetype = "image/*"
            }
            "video"-> {
                currentFilePath = LocalSingelton.finalVideoPath
                sharetype = "video/*"
            }
        }
        Glide.with(this)
            .load(currentFilePath)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .placeholder(R.drawable.ic_camera)
            .into(binding?.finalImage!!)

        binding?.filepathtext?.text = LocalSingelton.finalfilepath

        setAllClick(from)
    }

    private fun setAllClick(from: String?) {
        binding?.backbtn?.setOnClickListener {
            startActivity(Intent(this, CameraActivity::class.java))
            finish()
        }

        binding?.homebtn?.setOnClickListener {
            finish()
        }

        binding?.finalImage?.setOnClickListener {
            when(from)
            {
                "image"-> {
                    startActivity(Intent(this, ImageViewActivity::class.java).putExtra("fromGallery", true))
                }
                "video"-> {
                    startActivity(Intent(this, VideoViewActivity::class.java).putExtra("fromGallery", true))
                }
            }
        }
    }

    fun sharewhatsapp(view: android.view.View) {
        try {
            val whatsappshare = Intent(Intent.ACTION_SEND)
            whatsappshare.type = sharetype
            whatsappshare.setPackage("com.whatsapp")
            whatsappshare.putExtra(Intent.EXTRA_STREAM, LocalSingelton.currentUri)
            startActivity(whatsappshare)
        } catch (e: Exception) {
            Toast.makeText(this, "App not found", Toast.LENGTH_LONG).show()
        }
    }

    fun sharefacebook(view: android.view.View) {
        try {
            val facebookshare = Intent(Intent.ACTION_SEND)
            facebookshare.type = sharetype
            facebookshare.setPackage("com.facebook.katana")
            facebookshare.putExtra(Intent.EXTRA_STREAM, LocalSingelton.currentUri)
            startActivity(facebookshare)
        } catch (e: Exception) {
            Toast.makeText(this, "App not found", Toast.LENGTH_LONG).show()
        }
    }

    fun shareinstagram(view: android.view.View) {
        try {
            val instagramshare = Intent(Intent.ACTION_SEND)
            instagramshare.type = sharetype
            instagramshare.setPackage("com.instagram.android")
            instagramshare.putExtra(Intent.EXTRA_STREAM, LocalSingelton.currentUri)
            startActivity(instagramshare)
        } catch (e: Exception) {
            Toast.makeText(this, "App not found", Toast.LENGTH_LONG).show()
        }
    }

    fun sharemore(view: android.view.View) {
        try {
            val share = Intent(Intent.ACTION_SEND)
            share.type = sharetype
            share.putExtra(Intent.EXTRA_STREAM, LocalSingelton.currentUri)
            startActivity(share)
        } catch (e: Exception) {
            Toast.makeText(this, "App not found", Toast.LENGTH_LONG).show()
        }
    }

    private fun setNativeAd() {
        val builder = AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
        builder.forNativeAd { unifiedNativeAd: NativeAd ->
            if (nativeAd != null) {
                nativeAd?.destroy()
            }
            nativeAd = unifiedNativeAd
            val adView = NativeadviewLayoutBinding.inflate(LayoutInflater.from(this)).root
            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            binding?.adlayout?.removeAllViews()
            binding?.adlayout?.addView(adView)
        }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding?.defaultImage?.setVisibility(View.GONE)
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                getUrl()
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        adView.setMediaView(mediaView)
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline))
        adView.setBodyView(adView.findViewById(R.id.ad_body))
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action))
        adView.setIconView(adView.findViewById(R.id.ad_app_icon))
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView.visibility = View.INVISIBLE
        } else {
            adView.bodyView.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView.visibility = View.INVISIBLE
        } else {
            adView.callToActionView.visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon.drawable
            )
            adView.iconView.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }

    private fun getUrl() {
        RetrofitClient.getCustomNativeAdClient()?.getBtnAd()?.enqueue(object :
            Callback<Model_Btn?> {
            override fun onResponse(call: Call<Model_Btn?>, response: Response<Model_Btn?>) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body()!!.isStatus()) {
                            if (response.body()!!.getData() != null) {
                                checkqureka = response.body()!!.getData().isFlage()
                                if (checkqureka) {
                                    qurekaimage = response.body()!!.getData().getImage()
                                    querekatext = response.body()!!.getData().getTitle()
                                    url = response.body()!!.getData().getUrl()
                                    qureka()
                                }
                            }
                        }
                    }
                }
            }
            override fun onFailure(call: Call<Model_Btn?>, t: Throwable) {}
        })
    }

    private fun qureka() {
        try {
            val querekanativebinding =  QurekaNativeAdsBinding.inflate(layoutInflater)

            try {
                if (qurekaimage != "") {
                    Glide.with(this).load(qurekaimage).into(querekanativebinding.adAppIcon)
                    querekanativebinding.adHeadline.setText(querekatext)
                    querekanativebinding.adCallToAction.setText(querekatext)
                    binding?.adlayout?.removeAllViews()
                    binding?.adlayout?.addView(querekanativebinding.root)
                    binding?.defaultImage?.visibility = View.GONE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            querekanativebinding.adCallToAction.setOnClickListener { view1: View? ->
                try {
                    val builder = CustomTabsIntent.Builder()
                    val customTabsIntent = builder.build()
                    customTabsIntent.intent.setPackage("com.android.chrome")
                    customTabsIntent.launchUrl(this, Uri.parse(url))
                    MyApplication.appOpenManager?.isAdShow = true
                } catch (e: Exception) {
                    val builder = CustomTabsIntent.Builder()
                    val customTabsIntent = builder.build()
                    customTabsIntent.launchUrl(this, Uri.parse(url))
                }
            }
        } catch (e: Exception) {
        }
    }

}