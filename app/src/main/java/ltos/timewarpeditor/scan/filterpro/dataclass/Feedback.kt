package ltos.timewarpeditor.scan.filterpro.dataclass

data class Feedback(val status: Boolean, val message: String)

