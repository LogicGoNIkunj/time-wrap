package ltos.timewarpeditor.scan.filterpro

import ltos.timewarpeditor.scan.filterpro.singleton.*
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val singlemodule = module {
    single { FileUtility(androidContext()) }
    single { DeviceScreenInfo(androidContext()) }
    single { LocalDataStore(androidContext()) }
    single { MyCameraX(androidContext(), get()) }
    single { MyTouchListener() }
    single { FrameToVideo() }

}
val bitmapModule = module {
    single { BitmapProcessor(get()) }
}