package ltos.timewarpeditor.scan.filterpro.activity

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import ltos.timewarpeditor.scan.filterpro.bitmapModule
import ltos.timewarpeditor.scan.filterpro.databinding.ActivityMainBinding
import ltos.timewarpeditor.scan.filterpro.iinterface.InvokeActivityMethod
import ltos.timewarpeditor.scan.filterpro.sealed.Capturemode
import ltos.timewarpeditor.scan.filterpro.singleton.*
import ltos.timewarpeditor.scan.filterpro.utiltiy.MyApplication
import kotlinx.coroutines.*
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.core.context.GlobalContext.loadKoinModules
import org.koin.core.context.GlobalContext.unloadKoinModules
import java.io.FileOutputStream

class CameraActivity : AppCompatActivity(), InvokeActivityMethod {

    var binding: ActivityMainBinding? = null
    private val localDataStore: LocalDataStore by inject()
    private val myCametaX: MyCameraX by inject()
    private val myTouchListener: MyTouchListener by inject()

    private var bitmapProcessor: BitmapProcessor = getKoin().get()
    private val fileUtility: FileUtility by inject()
    private val frameToVideo: FrameToVideo by inject()
    private var videoCaptureMode = false
    private var job: Job? = null

    private var mInterstitialAd: InterstitialAd? = null
    private var isActivityLeft = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        myTouchListener.capturemode = Capturemode.stop

        binding?.texturView?.isOpaque = false

        LoadIntersstaticialAd()

        intent.getStringExtra("internal")?.let {
            unloadKoinModules(bitmapModule)
            loadKoinModules(bitmapModule)
            bitmapProcessor = getKoin().get()
        }

        CoroutineScope(Dispatchers.IO).launch {
            startCameaWithCoroutine()
            cancel()
        }
        setSwipeDetection()
        setAllClick()
    }

    suspend private fun startCameaWithCoroutine() {
        myCametaX.startCamera(this@CameraActivity)
    }

    private fun setAllClick() {
        binding?.flash?.setOnClickListener {
            myCametaX.onOffTorch(binding?.flash!!)
        }
        binding?.switchCamera?.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                when (localDataStore.getStringfromDataStore("cameraFacing")) {
                    "FRONT" -> {
                        localDataStore.setStringInDataStore("cameraFacing", "BACK")
                        startCameaWithCoroutine()
                    }
                    "BACK" -> {
                        localDataStore.setStringInDataStore("cameraFacing", "FRONT")
                        startCameaWithCoroutine()
                    }
                }
                cancel()
            }
        }
        binding?.imagecapturemode?.setOnClickListener {
            videoCaptureMode = false
            binding?.imagecapturemode?.imageTintList = ColorStateList.valueOf(Color.MAGENTA)
            binding?.videocapturemode?.imageTintList = null
        }
        binding?.videocapturemode?.setOnClickListener {
            videoCaptureMode = true
            binding?.imagecapturemode?.imageTintList = null
            binding?.videocapturemode?.imageTintList = ColorStateList.valueOf(Color.MAGENTA)
        }
    }

    /* fun takePhoto(view: android.view.View) {
         // Get a stable reference of the modifiable image capture use case

         // Create time-stamped output file to hold the image
         val photoFile = File(
             FileUtility.AppFolderName,
             SimpleDateFormat(
                 LocalSingelton.FILE_FORMAT, Locale.ENGLISH
             ).format(System.currentTimeMillis()) + ".jpg")

         // Create output options object which contains file + metadata
         val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

         // Set up image capture listener, which is triggered after photo has
         // been taken
         imageCapture?.takePicture(
             outputOptions, ContextCompat.getMainExecutor(this), object : ImageCapture.OnImageSavedCallback {
                 override fun onError(exc: ImageCaptureException) {
                     Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                 }
                 override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                     val savedUri = Uri.fromFile(photoFile)
                     val msg = "Photo capture succeeded: $savedUri"
                     Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                     Log.d(TAG, msg)
                 }
             })
     }*/

    private fun setSwipeDetection() {
        myTouchListener.listener = this
        binding?.viewFinder?.setOnTouchListener(myTouchListener)
    }

    suspend private fun startImageCapture() {
        var bitmapForSize: Bitmap?
        bitmapProcessor.paint?.setColor(Color.parseColor(localDataStore.getStringfromDataStore("colorforline")))
        withContext(Dispatchers.Main)
        {
            bitmapForSize = binding?.viewFinder?.bitmap
        }
        when (myTouchListener.isVerical) {
            true -> {
                val divisionPercentile =
                    ((localDataStore.getStringfromDataStore("vFrameSize")).toInt()+2) * 0.001
                bitmapProcessor.frameSize = (bitmapForSize?.height!! * divisionPercentile).toInt()
            }
            false -> {
                val divisionPercentile =
                    ((localDataStore.getStringfromDataStore("hFrameSize")).toInt()+2) * 0.001
                bitmapProcessor.frameSize = (bitmapForSize?.width!! * divisionPercentile).toInt()
            }
        }
        if (videoCaptureMode) {
            val filepath = fileUtility.getTempVideoFile().path
            frameToVideo.setRecorder(filepath,
                bitmapForSize?.width,
                bitmapForSize?.height)
            LocalSingelton.finalVideoPath = filepath
        }
        bitmapProcessor.currentBitmap = Bitmap.createBitmap(bitmapForSize?.width!!,
            bitmapForSize?.height!!,
            Bitmap.Config.ARGB_8888)
        bitmapForSize!!.recycle()

        var previewBitmap: Bitmap?
        bitmapProcessor.notMaximumPoint = true
        var loop = 0

        while (bitmapProcessor.notMaximumPoint) {
            withContext(Dispatchers.Main)
            {
                previewBitmap = binding?.viewFinder?.bitmap
            }
            Log.e("TAG", "startImageCapture: ${++loop}", )
            val croppedBitmap = bitmapProcessor.cropBitmap(previewBitmap)
            bitmapProcessor.setBitmaptoTextureView(croppedBitmap)
            val canvas = binding?.texturView?.lockCanvas()
            canvas?.drawBitmap(bitmapProcessor.currentBitmap!!, 0F, 0F, null)
            binding?.texturView?.unlockCanvasAndPost(canvas!!)
            if (videoCaptureMode) {
                val canvas1 = Canvas(previewBitmap!!)
                canvas1.drawBitmap(bitmapProcessor.currentBitmap!!, 0F, 0F, null)
                frameToVideo.addFrame(previewBitmap!!)
            }
            previewBitmap?.recycle()
        }
        if (!videoCaptureMode) {
            saveBitmaptoTempFile()
        }
        if (videoCaptureMode) {
            frameToVideo.clearRecorder()
        }
        withContext(Dispatchers.Main)
        {
            launch {
                when (videoCaptureMode) {
                    true -> {
                        val nextActivity =
                            Intent(this@CameraActivity, VideoViewActivity::class.java)
                        startActivity(nextActivity)
                        showInterstaticial()
                        finish()
                    }
                    false -> {
                        val nextActivity =
                            Intent(this@CameraActivity, ImageViewActivity::class.java)
                        startActivity(nextActivity)
                        showInterstaticial()
                        finish()
                    }
                }
            }
        }
        myTouchListener.capturemode = Capturemode.stop
    }

    private fun saveBitmaptoTempFile() {
        LocalSingelton.finalBitmapPath = fileUtility.getTempFile().path
        val fOut = FileOutputStream(LocalSingelton.finalBitmapPath)
        binding?.texturView?.bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
        fOut.flush()
        fOut.close()
    }

    override fun invokeStartImageCapture() {
        if (myTouchListener.capturemode == Capturemode.stop) {
            myTouchListener.capturemode = Capturemode.running
            hideAllView()
            job = CoroutineScope(Dispatchers.IO).async {
                startImageCapture()
            }
        }
    }

    private fun hideAllView() {
        binding?.switchCamera?.visibility = View.GONE
        binding?.flash?.visibility = View.GONE
        binding?.verticalAnim?.visibility = View.GONE
        binding?.horizontalAnim?.visibility = View.GONE
        binding?.videocapturemode?.visibility = View.GONE
        binding?.imagecapturemode?.visibility = View.GONE
    }

    override fun onBackPressed() {
        when (myTouchListener.capturemode) {
            Capturemode.running -> {
                startActivity(Intent(this, CameraActivity::class.java).putExtra("internal", "true"))
                finish()
            }
            Capturemode.stop -> finish()
        }
    }

    private fun LoadIntersstaticialAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.setFullScreenContentCallback(object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            super.onAdDismissedFullScreenContent()
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            super.onAdFailedToShowFullScreenContent(adError)
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            MyApplication.appOpenManager!!.isAdShow = true
                        }
                    })
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    MyApplication.appOpenManager!!.isAdShow = false
                }
            })
    }

    private fun showInterstaticial() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd!!.show(this)
                MyApplication.appOpenManager!!.isAdShow = true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        isActivityLeft = false
    }

    override fun onResume() {
        super.onResume()
        isActivityLeft = false
        unloadKoinModules(bitmapModule)
        loadKoinModules(bitmapModule)
        bitmapProcessor = getKoin().get()
    }

    override fun onPause() {
        super.onPause()
        isActivityLeft = true
    }

    override fun onStop() {
        super.onStop()
        isActivityLeft = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isActivityLeft = true

        job?.cancel()
    }
}