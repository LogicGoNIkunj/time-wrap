package ltos.timewarpeditor.scan.filterpro.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.content.res.AppCompatResources
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.onesignal.OneSignal
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.api.RetrofitClient
import ltos.timewarpeditor.scan.filterpro.databinding.ActivityPreCameraBinding
import ltos.timewarpeditor.scan.filterpro.databinding.FragmentExitBinding
import ltos.timewarpeditor.scan.filterpro.databinding.NativeadviewLayoutBinding
import ltos.timewarpeditor.scan.filterpro.databinding.RepermissiondialogBinding
import ltos.timewarpeditor.scan.filterpro.dataclass.Model_Btn
import ltos.timewarpeditor.scan.filterpro.singleton.LocalSingelton
import ltos.timewarpeditor.scan.filterpro.utiltiy.MyApplication
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class PreCameraActivity : AppCompatActivity() {

    private var binding: ActivityPreCameraBinding? = null
    private var exitBinding: FragmentExitBinding? = null
    private var exitDialog: AlertDialog? = null

    //Ad Variable
    private var nativeAd: NativeAd? = null
    private var exitnativeAd: NativeAd? = null
    private var qurekaimage = ""
    private var querekatext = ""
    private var url = ""
    private var checkqureka = false
    private var isActivityLeft = false
    private var mInterstitialAd: InterstitialAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPreCameraBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        Analytics()
        setNativeAd()
        showBanner()

        setAllClick()
        createExitDialog()
    }

    private fun Analytics() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        FirebaseCrashlytics.getInstance()
        FirebaseAnalytics.getInstance(this)
        OneSignal.initWithContext(this)
        OneSignal.setAppId(resources.getString(R.string.onesignal))
    }

    private fun setAllClick() {
        binding?.camerabtn?.setOnClickListener {
            when (checkManifiestPermission()) {
                true -> {
                    startActivity(Intent(this, CameraActivity::class.java))
                }
                false -> {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE),
                        LocalSingelton.MANIFIEST_PERMISSION_REQUESTCODE)
                }
            }
        }
        binding?.settingbtn?.setOnClickListener {
            startActivity(Intent(this, SettingActivity::class.java))
            showInterstaticial()
        }
        binding?.gallerybtn?.setOnClickListener {
            startActivity(Intent(this, MyCreationActivity::class.java))
            showInterstaticial()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var permitted = true
        when (requestCode) {
            LocalSingelton.MANIFIEST_PERMISSION_REQUESTCODE -> {
                for (i in grantResults) {
                    if (i != PackageManager.PERMISSION_GRANTED) {
                        permitted = false
                        if (i == PackageManager.PERMISSION_DENIED) {
                            showRepermissionDialog()
                        }
                        Log.e("TAG", "onRequestPermissionsResult:$i ")
                        break
                    }
                }
                when (permitted) {
                    true -> {
                        startActivity(Intent(this, CameraActivity::class.java))
                        showInterstaticial()
                    }
                    false -> {}
                }
            }
        }
    }

    private fun showRepermissionDialog() {
        val repermissionbinding = RepermissiondialogBinding.inflate(layoutInflater)

        val repermissiondialog = AlertDialog.Builder(this)
            .setView(repermissionbinding.root)
            .create()

        repermissiondialog.window?.setBackgroundDrawable(AppCompatResources.getDrawable(this,
            R.drawable.roundcorner_bg))

        repermissiondialog.show()

        repermissiondialog.getWindow()
            ?.setLayout((resources.displayMetrics.widthPixels * 0.8).toInt(),
                WindowManager.LayoutParams.WRAP_CONTENT)

        repermissionbinding.cancel.setOnClickListener {
            repermissiondialog.dismiss()
        }
        repermissionbinding.opensetting.setOnClickListener {
            try {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                intent.data = Uri.parse("package:$packageName")
                startActivity(intent)
                repermissiondialog.dismiss()
            } catch (e: Exception) {
            }
        }
    }

    private fun checkManifiestPermission(): Boolean {

        val camera = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        )
        val write = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val read = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        return camera == PackageManager.PERMISSION_GRANTED
                && write == PackageManager.PERMISSION_GRANTED
                && read == PackageManager.PERMISSION_GRANTED
    }

    fun setNativeAd() {
        val builder = AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
        builder.forNativeAd { unifiedNativeAd: NativeAd ->
            if (nativeAd != null) {
                nativeAd?.destroy()
            }
            nativeAd = unifiedNativeAd
            val adView = NativeadviewLayoutBinding.inflate(layoutInflater).root
            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            binding?.adlayout?.removeAllViews()
            binding?.adlayout?.addView(adView)
        }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding?.defaultImage?.setVisibility(View.GONE)
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                getUrl()
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        adView.setMediaView(mediaView)
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline))
        adView.setBodyView(adView.findViewById(R.id.ad_body))
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action))
        adView.setIconView(adView.findViewById(R.id.ad_app_icon))
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView.visibility = View.INVISIBLE
        } else {
            adView.bodyView.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView.visibility = View.INVISIBLE
        } else {
            adView.callToActionView.visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon.drawable
            )
            adView.iconView.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }

    private fun getUrl() {
        RetrofitClient.getCustomNativeAdClient()?.getBtnAd()?.enqueue(object :
            Callback<Model_Btn?> {
            override fun onResponse(call: Call<Model_Btn?>, response: Response<Model_Btn?>) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body()!!.isStatus()) {
                            if (response.body()!!.getData() != null) {
                                checkqureka = response.body()!!.getData().isFlage()
                                if (checkqureka) {
                                    qurekaimage = response.body()!!.getData().getImage()
                                    querekatext = response.body()!!.getData().getTitle()
                                    url = response.body()!!.getData().getUrl()
                                    qureka()
                                }
                            }
                        }
                    }
                }
            }
            override fun onFailure(call: Call<Model_Btn?>, t: Throwable) {}
        })
    }

    fun qureka() {
        val view = layoutInflater.inflate(R.layout.qureka_native_ads, null)
        val ad_call_to_action = view.findViewById<TextView>(R.id.ad_call_to_action)
        val gifimage = view.findViewById<ImageView>(R.id.ad_app_icon)
        try {
            if (qurekaimage != "") {
                Glide.with(this).load(qurekaimage).into(gifimage)
                (view.findViewById<View>(R.id.ad_headline) as TextView).setText(querekatext)
                ad_call_to_action.setText(querekatext)
                binding?.adlayout?.removeAllViews()
                binding?.adlayout?.addView(view)
                binding?.defaultImage?.visibility = View.GONE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        ad_call_to_action.setOnClickListener { view1: View? ->
            try {
                val builder = CustomTabsIntent.Builder()
                val customTabsIntent = builder.build()
                customTabsIntent.intent.setPackage("com.android.chrome")
                customTabsIntent.launchUrl(this, Uri.parse(url))
            } catch (e: Exception) {
                val builder = CustomTabsIntent.Builder()
                val customTabsIntent = builder.build()
                customTabsIntent.launchUrl(this, Uri.parse(url))
            }
        }
    }

    private fun LoadIntersstaticialAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.fullScreenContentCallback = object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            super.onAdDismissedFullScreenContent()
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            super.onAdFailedToShowFullScreenContent(adError)
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            MyApplication.appOpenManager!!.isAdShow = true
                        }
                    }
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    MyApplication.appOpenManager!!.isAdShow = false
                }
            })
    }

    private fun showInterstaticial() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd!!.show(this)
                MyApplication.appOpenManager!!.isAdShow = true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        isActivityLeft = false
    }

    override fun onResume() {
        super.onResume()
        isActivityLeft = false
        LoadIntersstaticialAd()
    }

    override fun onPause() {
        super.onPause()
        isActivityLeft = true
    }

    override fun onStop() {
        super.onStop()
        isActivityLeft = true
    }

    override fun onDestroy() {
        super.onDestroy()
        if (nativeAd != null) {
            nativeAd?.destroy()
        }

        if (exitnativeAd != null) {
            exitnativeAd?.destroy()
        }
        isActivityLeft = true
    }

    override fun onBackPressed() {
        exitDialog?.show()
    }

    private fun createExitDialog() {
        if (exitBinding == null) {
            exitBinding = FragmentExitBinding.inflate(layoutInflater)
            exitBinding?.btnPositive?.setOnClickListener {
                finishAffinity()
            }
            exitBinding?.btnNegative?.setOnClickListener {
                exitDialog?.dismiss()
            }
            exitDialog = AlertDialog.Builder(this)
                .setView(exitBinding?.root)
                .setCancelable(false)
                .create()
                .apply {
                    window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@PreCameraActivity,
                        R.drawable.roundcorner_bg))
                }
            exitsetNativeAd()
        }
    }

    private fun exitsetNativeAd() {
        val builder = AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
        builder.forNativeAd { unifiedNativeAd: NativeAd ->
            if (exitnativeAd != null) {
                exitnativeAd?.destroy()
            }
            exitnativeAd = unifiedNativeAd
            val adView = NativeadviewLayoutBinding.inflate(layoutInflater).root
            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            exitBinding?.adlayout?.removeAllViews()
            exitBinding?.adlayout?.addView(adView)
        }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                exitBinding?.defaultImage?.setVisibility(View.GONE)
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                exitgetUrl()
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun exitgetUrl() {
        RetrofitClient.getCustomNativeAdClient()?.getBtnAd()?.enqueue(object :
            Callback<Model_Btn?> {
            override fun onResponse(call: Call<Model_Btn?>, response: Response<Model_Btn?>) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body()!!.isStatus()) {
                            if (response.body()!!.getData() != null) {
                                checkqureka = response.body()!!.getData().isFlage()
                                if (checkqureka) {
                                    qurekaimage = response.body()!!.getData().getImage()
                                    querekatext = response.body()!!.getData().getTitle()
                                    url = response.body()!!.getData().getUrl()
                                    exitqureka()
                                }
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<Model_Btn?>, t: Throwable) {}
        })
    }

    private fun exitqureka() {
        val view = layoutInflater.inflate(R.layout.qureka_native_ads, null)
        val ad_call_to_action = view.findViewById<TextView>(R.id.ad_call_to_action)
        val gifimage = view.findViewById<ImageView>(R.id.ad_app_icon)
        try {
            if (qurekaimage != "") {
                Glide.with(this).load(qurekaimage).into(gifimage)
                (view.findViewById<View>(R.id.ad_headline) as TextView).setText(querekatext)
                ad_call_to_action.setText(querekatext)
                exitBinding?.adlayout?.removeAllViews()
                exitBinding?.adlayout?.addView(view)
                exitBinding?.defaultImage?.visibility = View.GONE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        ad_call_to_action.setOnClickListener { view1: View? ->
            try {
                val builder = CustomTabsIntent.Builder()
                val customTabsIntent = builder.build()
                customTabsIntent.intent.setPackage("com.android.chrome")
                customTabsIntent.launchUrl(this, Uri.parse(url))
                MyApplication.appOpenManager?.isAdShow = true
            } catch (e: Exception) {
                val builder = CustomTabsIntent.Builder()
                val customTabsIntent = builder.build()
                customTabsIntent.launchUrl(this, Uri.parse(url))
            }
        }
    }

    private fun showBanner() {
        val adView = AdView(this)
        adView.adUnitId = Objects.requireNonNull(MyApplication.get_Admob_banner_Id())
        val adSize = getAdSize()
        adView.adSize = adSize
        adView.adListener = object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                binding?.adtext?.visibility = View.GONE
            }

            override fun onAdLoaded() {
                super.onAdLoaded()
                binding?.bannerad?.addView(adView)
                binding?.adtext?.visibility = View.GONE
            }
        }
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun getAdSize(): AdSize {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        val windowmNger = getSystemService(WINDOW_SERVICE) as WindowManager
        val display = windowmNger.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)
        val widthPixels = outMetrics.widthPixels.toFloat()
        val density = outMetrics.density
        val adWidth = (widthPixels / density).toInt()

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth)
    }

}