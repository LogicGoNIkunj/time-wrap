package ltos.timewarpeditor.scan.filterpro.adapter

import android.content.Context
import android.content.Intent
import android.media.MediaScannerConnection
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.activity.ImageViewActivity
import ltos.timewarpeditor.scan.filterpro.activity.VideoViewActivity
import ltos.timewarpeditor.scan.filterpro.databinding.SinglelayoutBinding
import ltos.timewarpeditor.scan.filterpro.iinterface.PowerOfAdapterToActivity
import ltos.timewarpeditor.scan.filterpro.singleton.LocalSingelton
import java.io.File

class FileAdapter: RecyclerView.Adapter<FileAdapter.ViewHolder>() {

    var filelist: MutableList<File>? = null
    private var context: Context? = null
    var listener:PowerOfAdapterToActivity? = null

    class ViewHolder(val binding: SinglelayoutBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = ltos.timewarpeditor.scan.filterpro.databinding.SinglelayoutBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentfile = filelist?.get(position)
        Glide.with(context!!)
            .load(currentfile)
            .placeholder(R.drawable.splashicon)
            .into(holder.binding.screenshot)

        holder.binding.deletebtn.setOnClickListener {
            listener?.deleteFile(currentfile, position)
        }

        holder.binding.sharebtn.setOnClickListener {
            shareFile(currentfile?.path)
        }

        holder.binding.screenshot.setOnClickListener {
            if(currentfile?.name?.contains(".jpg") == true) {
                LocalSingelton.finalBitmapPath = currentfile.path
                val nextActivity = Intent(context, ImageViewActivity::class.java).putExtra("fromGallery", true)
                context?.startActivity(nextActivity)
            }
            else if (currentfile?.name?.contains(".mp4") == true) {
                LocalSingelton.finalVideoPath = currentfile.path
                val nextActivity = Intent(context, VideoViewActivity::class.java).putExtra("fromGallery", true)
                context?.startActivity(nextActivity)
            }
        }
    }

    override fun getItemCount(): Int {
        return filelist?.size?:0
    }

    private fun shareFile(path: String?)
    {
        try {
            MediaScannerConnection.scanFile(
                context,
                arrayOf(path),
                null
            ) { s, uri -> //
                if(uri != null)
                {
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "*/*"
                    share.putExtra(Intent.EXTRA_STREAM, uri)
                    context?.startActivity(share)
                }
                else
                {
                    Toast.makeText(context, "Something went wrong.", Toast.LENGTH_SHORT).show()
                }
            }
        } catch (e: Exception) {

        }
    }
}