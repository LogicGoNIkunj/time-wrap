package ltos.timewarpeditor.scan.filterpro.singleton

import android.content.ContentValues
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BitmapProcessor(
    val myTouchListener: MyTouchListener
) {
    private var _yPosition = 0
    private var _xPosition = 0
    var currentBitmap: Bitmap? = null
    var frameSize = 0
    var notMaximumPoint = true
    var paint: Paint? = null

    init { paint = Paint() }

    suspend fun cropBitmap(previewBitmap: Bitmap?): Bitmap {
        try {
            if (myTouchListener.isVerical) {
                if (_yPosition >= previewBitmap?.height!!) {
                    notMaximumPoint = false
                } else {
                    val textureViewBitmap = previewBitmap.let {
                        Bitmap.createBitmap(
                            it,
                            0,
                            _yPosition,
                            previewBitmap.width,
                            frameSize
                        )
                    }
                    return textureViewBitmap
                }

            } else {
                if (_xPosition >= previewBitmap?.width!!) {
                    notMaximumPoint = false
                } else {
                    val textureViewBitmap = previewBitmap.let {
                        Bitmap.createBitmap(
                            it,
                            _xPosition,
                            0,
                            frameSize,
                            previewBitmap.height
                        )
                    }
                    return textureViewBitmap
                }
            }
        } catch (e: Exception) {
            Log.e(ContentValues.TAG, "cropBitmap: ${e.message}")
        }
        return previewBitmap!!
    }

    suspend fun setBitmaptoTextureView(newBitmap: Bitmap?) {
        withContext(Dispatchers.IO)
        {
            if(myTouchListener.isVerical)
            {
                val bitmapcanvas = currentBitmap?.let { Canvas(it) }
                newBitmap?.let {
                    bitmapcanvas?.drawBitmap(it, 0F, _yPosition.toFloat(), null)
                    _yPosition += frameSize
                    bitmapcanvas?.drawRect(0F, (_yPosition).toFloat(), it.width.toFloat(), (_yPosition+frameSize*5).toFloat(), paint!!)
                }
            }
            else
            {
                val bitmapcanvas = currentBitmap?.let { Canvas(it) }
                newBitmap?.let {
                    bitmapcanvas?.drawBitmap(it, _xPosition.toFloat(), 0F, null)
                    _xPosition += frameSize
                    bitmapcanvas?.drawRect((_xPosition).toFloat(), 0F, (_xPosition+frameSize*5).toFloat(), it.height.toFloat(), paint!!)
                }
            }
        }
    }
}