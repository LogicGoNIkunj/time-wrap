package ltos.timewarpeditor.scan.filterpro.activity

import android.content.Intent
import android.content.IntentSender
import android.media.MediaScannerConnection
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.provider.MediaStore
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.adapter.FileAdapter
import ltos.timewarpeditor.scan.filterpro.databinding.ActivityMyCreationBinding
import ltos.timewarpeditor.scan.filterpro.databinding.DeletedialogBinding
import ltos.timewarpeditor.scan.filterpro.databinding.SmallNative1Binding
import ltos.timewarpeditor.scan.filterpro.iinterface.PowerOfAdapterToActivity
import ltos.timewarpeditor.scan.filterpro.singleton.FileUtility
import ltos.timewarpeditor.scan.filterpro.utiltiy.MyApplication
import org.koin.android.ext.android.inject
import java.io.File
import java.util.*

class MyCreationActivity : AppCompatActivity(), PowerOfAdapterToActivity {

    private var binding: ActivityMyCreationBinding? = null
    private val fileUtility: FileUtility by inject()
    private var fileAdapter:FileAdapter? = null
    private var mLastClickTime = 0L
    private var currentposition = 0

    private var smallnativeAd: NativeAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyCreationBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        setsmallNativeAd()
        setRecyclerView()
        setAllClick()
    }

    private fun setAllClick() {
        binding?.sbg?.setOnPositionChangedListener { position ->
            if (position == 0) {
                fileAdapter?.filelist = fileUtility.getAllFile().filter {
                    it.name.contains("jpg")
                }as MutableList<File>?
                when(fileAdapter?.filelist?.size)
                {
                    null, 0 -> binding?.nodataimage?.visibility = View.VISIBLE
                    else -> binding?.nodataimage?.visibility = View.GONE
                }
                fileAdapter?.notifyDataSetChanged()

            } else if (position == 1) {
                fileAdapter?.filelist = fileUtility.getAllFile().filter {
                    it.name.contains("mp4")
                }as MutableList<File>?
                when(fileAdapter?.filelist?.size)
                {
                    null, 0 -> binding?.nodataimage?.visibility = View.VISIBLE
                    else -> binding?.nodataimage?.visibility = View.GONE
                }
                fileAdapter?.notifyDataSetChanged()
            }
        }

        binding?.backbtn?.setOnClickListener {
            finish()
        }
    }

    private fun setRecyclerView() {
        fileAdapter = FileAdapter()
        fileAdapter?.filelist = fileUtility.getAllFile().filter {
            it.name.contains("jpg")
        }as MutableList<File>?

        fileAdapter?.listener = this
        when(fileAdapter?.filelist?.size)
        {
            null, 0 -> binding?.nodataimage?.visibility = View.VISIBLE
            else -> binding?.nodataimage?.visibility = View.GONE
        }
        binding?.rcv?.apply {
            adapter = fileAdapter
        }
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(smallnativeAd!=null)
        {
            smallnativeAd?.destroy()
        }
    }

    override fun deleteFile(currentfile: File?, position: Int) {
        currentposition = position
        showDeleteDialog(currentfile)
    }

    fun showDeleteDialog(currentfile: File?) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            MediaScannerConnection.scanFile(
                this,
                arrayOf(currentfile?.path),
                null
            ) { s, uri -> //
                if (uri != null) {
                    val intent =
                        MediaStore.createDeleteRequest(contentResolver, setOf(uri))
                    try {
                        startIntentSenderForResult(intent.intentSender, 2, null, 0, 0, 0, null)
                    } catch (e: IntentSender.SendIntentException) {
                        e.printStackTrace()
                    }
                } else {
                    Toast.makeText(this, "Something went wrong.", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            val deletedialogBinding = DeletedialogBinding.inflate(layoutInflater)

            val deletedialog = AlertDialog.Builder(this)
                .setView(deletedialogBinding.root)
                .create()
                .apply {
                    window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@MyCreationActivity,R.drawable.roundcorner_bg))
                }
            deletedialog.show()

            deletedialog.getWindow()?.setLayout((resources.displayMetrics.widthPixels*0.8).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)

            deletedialogBinding.no.setOnClickListener {
                deletedialog.dismiss()
            }
            deletedialogBinding.yes.setOnClickListener {
                deletefile(currentfile)
                deletedialog.dismiss()
            }
        }
    }

    private fun updateRecyclerView() {
        fileAdapter?.notifyItemRemoved(currentposition)
        val tempList = LinkedList(fileAdapter?.filelist)
        tempList.removeAt(currentposition)
        fileAdapter?.filelist = tempList
        fileUtility.allFileList = tempList
        fileAdapter?.notifyItemRangeChanged(
            currentposition,
            fileAdapter?.filelist?.size!!
        )
        if(fileAdapter?.filelist?.size == 0)
        {
            binding?.nodataimage?.visibility = View.VISIBLE
        }
        Toast.makeText(this, "File Deleted successfully", Toast.LENGTH_SHORT).show()
    }


    private fun deletefile(currentfile: File?) {
        if(currentfile?.exists() == true)
        {
            if(currentfile.delete())
            {
                contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    "_data='" + currentfile.getPath() + "'",
                    null)
                updateRecyclerView()
            }
            else
            {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            2 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        updateRecyclerView()
                    }
                    RESULT_CANCELED -> {
                        Toast.makeText(this, "Allow to delete file", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun setsmallNativeAd() {
        val builder = AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
        builder.forNativeAd { unifiedNativeAd: NativeAd ->
            if (smallnativeAd != null) {
                smallnativeAd?.destroy()
            }
            smallnativeAd = unifiedNativeAd
            val adView = SmallNative1Binding.inflate(layoutInflater).root
            populateUnifiedSmallNativeAdView(unifiedNativeAd, adView)
            binding?.adlayout?.removeAllViews()
            binding?.adlayout?.addView(adView)
        }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding?.adtext?.visibility = View.GONE
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                binding?.adtext?.visibility = View.GONE
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    fun populateUnifiedSmallNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        /*val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        adView.setMediaView(mediaView)*/
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline))
        adView.setBodyView(adView.findViewById(R.id.ad_body))
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action))
        adView.setIconView(adView.findViewById(R.id.ad_app_icon))
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView?.visibility = View.INVISIBLE
        } else {
            adView.bodyView?.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView?.visibility = View.INVISIBLE
        } else {
            adView.callToActionView?.visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView?.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon?.drawable
            )
            adView.iconView?.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }
}