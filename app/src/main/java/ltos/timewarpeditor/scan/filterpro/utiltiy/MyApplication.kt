package ltos.timewarpeditor.scan.filterpro.utiltiy

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.gms.ads.MobileAds
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.api.AppOpenManager
import ltos.timewarpeditor.scan.filterpro.bitmapModule
import ltos.timewarpeditor.scan.filterpro.singlemodule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        MobileAds.initialize(this) { }

        context = this
        appOpenManager = AppOpenManager(this)
        preferences = getSharedPreferences("ps", MODE_PRIVATE)
        mEditor = preferences?.edit()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)


        startKoin {
             androidContext(this@MyApplication)
             modules(singlemodule, bitmapModule)
        }
    }

    companion object{
        var context: Context? = null
        var appOpenManager: AppOpenManager? = null
        var preferences: SharedPreferences? = null
        var mEditor: SharedPreferences.Editor? = null

        fun set_AdsInt(adsInt: Int) {
            mEditor!!.putInt("adsInt", adsInt).commit()
        }

        fun get_AdsInt(): Int {
            return preferences!!.getInt("adsInt", 3)
        }

        //admob interstitial
        fun set_Admob_interstitial_Id(Admob_interstitial_Id: String?) {
            mEditor!!.putString("Admob_interstitial_Id", Admob_interstitial_Id).commit()
        }

        fun get_Admob_interstitial_Id(): String? {
            return preferences!!.getString(
                "Admob_interstitial_Id",
                context?.getString(R.string.interstatatial_ad_id)
            )
        }

        //admob open
        fun set_Admob_banner_Id(Admob_banner_Id: String?) {
            mEditor!!.putString("Admob_banner_Id", Admob_banner_Id).commit()
        }

        fun get_Admob_banner_Id(): String? {
            return preferences!!.getString(
                "Admob_banner_Id",
                context?.getString(R.string.banner_ad_id)
            )
        }

        //admob native
        fun set_Admob_native_Id(Admob_native_Id: String?) {
            mEditor!!.putString("Admob_native_Id", Admob_native_Id).commit()
        }

        fun get_Admob_native_Id(): String? {
            return preferences!!.getString(
                "Admob_native_Id",
                context?.getString(R.string.native_ad_id)
            )
        }

        //admob openapp
        fun set_Admob_openapp(Admob_native_Id: String?) {
            mEditor!!.putString("Admob_open_Id", Admob_native_Id).commit()
        }

        fun get_Admob_openapp(): String? {
            return preferences!!.getString(
                "Admob_open_Id",
                context?.getString(R.string.open_ad_id)
            )
        }

    }
}