package ltos.timewarpeditor.scan.filterpro.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ltos.timewarpeditor.scan.filterpro.databinding.SinglecolorlayoutBinding
import ltos.timewarpeditor.scan.filterpro.iinterface.ColorAdapterToSettingActivity


class ColorAdapter: RecyclerView.Adapter<ColorAdapter.ViewHolder>() {

    private var context: Context? = null
    var colorlist: Array<String>? = null
    var listener:ColorAdapterToSettingActivity? = null


    class ViewHolder(val binding: SinglecolorlayoutBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = SinglecolorlayoutBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val hexcolorcode = colorlist!![position]

        if(hexcolorcode == "#FFFFFF")
        {
            holder.binding.colorpickersingle.backgroundTintList = null
        }
        else
        {
            holder.binding.colorpickersingle.backgroundTintList = ColorStateList.valueOf(Color.parseColor(hexcolorcode))
        }

        holder.binding.colorpickersingle.setOnClickListener {
            listener?.colorClick(hexcolorcode)
        }
    }

    override fun getItemCount(): Int {
        return  colorlist?.size?:0
    }
}