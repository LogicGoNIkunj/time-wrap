package ltos.timewarpeditor.scan.filterpro.singleton

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class LocalDataStore(val context: Context) {

    private val Context.dataStore by preferencesDataStore(LocalSingelton.LOCAL_DATASTORE_NAME)

    suspend fun getStringfromDataStore(key: String):String
    {
        return when(key)
        {
            "cameraFacing"->{
                context.dataStore.data.map {
                    it[stringPreferencesKey(key)]?:"BACK"
                }.first()
            }
            "hFrameSize"->{
                context.dataStore.data.map {
                    it[stringPreferencesKey(key)]?:"6"
                }.first()
            }
            "vFrameSize"->{
                context.dataStore.data.map {
                    it[stringPreferencesKey(key)]?:"4"
                }.first()
            }
            "colorforline"->{
                context.dataStore.data.map {
                    it[stringPreferencesKey(key)]?:"#FFFFFF"
                }.first()
            }
            else->{
                context.dataStore.data.map {
                    it[stringPreferencesKey(key)]?:""
                }.first()
            }
        }
    }

    suspend fun setStringInDataStore(key: String, value:String){
        context.dataStore.edit {
            it[stringPreferencesKey(key)] = value
        }
    }
}