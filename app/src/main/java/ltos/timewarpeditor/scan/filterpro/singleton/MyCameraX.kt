package ltos.timewarpeditor.scan.filterpro.singleton

import android.content.ContentValues
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.core.impl.CameraConfig
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import ltos.timewarpeditor.scan.filterpro.activity.CameraActivity
import ltos.timewarpeditor.scan.filterpro.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MyCameraX(val applicationContext: Context, val localDataStore: LocalDataStore) {

    private var camera:Camera? = null
    private var flashOn = false

     suspend fun startCamera(activity: CameraActivity) {
         var cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

         when(localDataStore.getStringfromDataStore("cameraFacing"))
         {
             "FRONT"->{
                 cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA
             }
         }
        withContext(Dispatchers.IO)
        {
                val cameraProviderFuture = ProcessCameraProvider.getInstance(activity)
                cameraProviderFuture.addListener(Runnable {
                    val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
                    // Preview
                    val preview = Preview.Builder()
                        .build()
                        .also {
                            it.setSurfaceProvider(activity.binding?.viewFinder?.surfaceProvider)
                        }
                    // Select back camera as a default

                    try {
                        // Unbind use cases before rebinding
                        cameraProvider.unbindAll()
                        // Bind use cases to camera
                        camera = cameraProvider.bindToLifecycle(
                            activity, cameraSelector, preview)
                        camera?.cameraControl?.cancelFocusAndMetering()

                    } catch(exc: Exception) {
                        Log.e(ContentValues.TAG, "Use case binding failed", exc)
                    }
                },  ContextCompat.getMainExecutor(activity))
        }

         showHideFlash(activity?.binding?.flash!!)
    }

    fun onOffTorch(flash: AppCompatImageView)
    {

        when(camera?.cameraInfo?.hasFlashUnit())
        {
            true-> {
                when(flashOn)
                {
                    true->{
                        camera?.cameraControl?.enableTorch(false)
                        flashOn = !flashOn
                        flash.setImageResource(R.drawable.ic_flashoff)
                    }
                    false->{
                        camera?.cameraControl?.enableTorch(true)
                        flashOn = !flashOn
                        flash.setImageResource(R.drawable.ic_flashon)
                    }
                }
            }
            false-> Toast.makeText(applicationContext, "Not supported feature", Toast.LENGTH_SHORT).show()
        }
    }

    suspend fun showHideFlash(flash: AppCompatImageView)
    {
        withContext(Dispatchers.Main)
        {
            when(camera?.cameraInfo?.hasFlashUnit())
            {
                true-> flash.visibility = View.VISIBLE
                false-> flash.visibility = View.GONE
            }
        }
    }
}