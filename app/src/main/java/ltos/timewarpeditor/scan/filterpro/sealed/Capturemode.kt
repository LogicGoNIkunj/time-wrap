package ltos.timewarpeditor.scan.filterpro.sealed

sealed class Capturemode{
    object running:Capturemode()
    object stop:Capturemode()
}
