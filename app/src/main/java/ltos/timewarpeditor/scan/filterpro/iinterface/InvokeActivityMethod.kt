package ltos.timewarpeditor.scan.filterpro.iinterface

interface InvokeActivityMethod {
    fun invokeStartImageCapture()
}