package ltos.timewarpeditor.scan.filterpro.activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.adapter.ColorAdapter
import ltos.timewarpeditor.scan.filterpro.api.RetrofitClient
import ltos.timewarpeditor.scan.filterpro.databinding.*
import ltos.timewarpeditor.scan.filterpro.dataclass.Feedback
import ltos.timewarpeditor.scan.filterpro.iinterface.ColorAdapterToSettingActivity
import ltos.timewarpeditor.scan.filterpro.singleton.FileUtility
import ltos.timewarpeditor.scan.filterpro.singleton.LocalDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingActivity : AppCompatActivity(), ColorAdapterToSettingActivity {

    private val localDataStore: LocalDataStore by inject()
    private val fileUtility: FileUtility by inject()

    private var binding: ActivitySettingBinding? = null
    private var saveLocation = ""

    private var ratebinding: RatedialogBinding? = null
    private var ratedialog: AlertDialog? = null
    private var feedbackdialog:AlertDialog? = null
    private var feddbackbinding:FragmentFeedbackBinding? = null
    private var isFatching = false

    private var colorBinding:ColorpickerlayoutBinding? = null
    private var colorDialog:AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        saveLocation = fileUtility.AppFolderName.toString()

        CoroutineScope(Dispatchers.Main).launch {
            setColorforColorPicker(localDataStore.getStringfromDataStore("colorforline"))
        }

        setAllSeekBar()
        setAllClick()
        setAllText()

        createColorDialog()
    }

    private fun setAllText() {
        try {
            binding?.appversion?.text = "App Vesion:  "+getPackageManager()
                .getPackageInfo(getPackageName(), 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun setAllClick() {

        binding?.shareappbtn?.setOnClickListener {
            val sendIntent = Intent(Intent.ACTION_SEND)
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                "Check out the App at: https://play.google.com/store/apps/details?id=${packageName}"
            )
            sendIntent.type = "text/plain"
            startActivity(Intent.createChooser(sendIntent, "Share Via"))
        }

        binding?.privacyPolicy?.setOnClickListener {
            if (isOnline()) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.privacy_policy_link))
                    )
                )
            } else {
                Toast.makeText(
                    this,
                    "You are not connected to Internet",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        binding?.storageBtn?.setOnClickListener {
            showLocationDialog()
        }

        binding?.rateappbtn?.setOnClickListener {
           showRatindDialog()
        }

        binding?.colorpicker?.setOnClickListener {
            showColorDialog()
        }

        binding?.backbtn?.setOnClickListener {
            finish()
        }
    }

    private fun createColorDialog()
    {
        if(colorBinding == null)
        {
            colorBinding = ColorpickerlayoutBinding.inflate(layoutInflater)

            val colorAdapter = ColorAdapter()
            colorAdapter.colorlist =  getResources().getStringArray(R.array.colorlist)
            colorAdapter.listener = this
            colorBinding?.rcvColorPicker?.apply {
                adapter = colorAdapter
            }

            colorDialog = AlertDialog.Builder(this)
                .setView(colorBinding?.root)
                .create()
        }
    }

    private fun showColorDialog()
    {
        colorDialog?.show()
    }

    private fun setAllSeekBar() {

        CoroutineScope(Dispatchers.Main).launch {
            binding?.seekbarVertical?.progress =
                localDataStore.getStringfromDataStore("vFrameSize").toInt()
            binding?.seekbarHorizontal?.progress =
                localDataStore.getStringfromDataStore("hFrameSize").toInt()
            cancel()
        }

        binding?.seekbarHorizontal?.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, position: Int, fromUser: Boolean) {
                if (fromUser) {
                    CoroutineScope(Dispatchers.IO).launch {
                        localDataStore.setStringInDataStore("hFrameSize", position.toString())
                        cancel()
                    }
                }
            }
            override fun onStartTrackingTouch(p0: SeekBar?) {
            }
            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })

        binding?.seekbarVertical?.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, position: Int, fromUser: Boolean) {
                if (fromUser) {
                    CoroutineScope(Dispatchers.IO).launch {
                        localDataStore.setStringInDataStore("vFrameSize", position.toString())
                        cancel()
                    }

                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })
    }

    override fun onBackPressed() {
        finish()
    }

    protected fun isOnline(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        @SuppressLint("MissingPermission") val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    private fun showLocationDialog() {
        AlertDialog.Builder(this)
            .setTitle("Save Location")
            .setMessage(saveLocation)
            .create()
            .apply {
                window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
            }
            .show()
    }

    private fun giveRattinf() {
//        val rating = ratebinding?.ratingBar?.rating?.toInt()

        try {
            val sb = "market://details?id=" + getPackageName()
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(sb)))
        } catch (unused: ActivityNotFoundException) {
            val sb2 =
                "https://play.google.com/store/apps/details?id=" + getPackageName()
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(sb2)))
        }
        /*else {
           activity?.supportFragmentManager
               ?.beginTransaction()
               ?.add(com.tv.casting.screenrecorder.R.id.frag_container, FeedbackFragment())
               ?.addToBackStack(null)
               ?.commit()
       }*/
    }

    private fun showRatindDialog() {
        ratebinding = RatedialogBinding.inflate(layoutInflater)
        ratedialog = AlertDialog.Builder(this)
            .setView(ratebinding?.root)
            .create()
            .apply {
                window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
            }
        ratebinding?.ratingBar?.setOnRatingBarChangeListener({ ratingBar, rating, fromUser ->
            if(fromUser)
            {
                if(rating == 4f || rating == 5f)
                {
                    giveRattinf()
                    ratedialog?.dismiss()
                }
                else
                {
                    showfeedbackdialog()
                    ratedialog?.dismiss()
                }
            }
        })
        ratebinding?.cancel?.setOnClickListener {
            ratedialog?.dismiss()
        }
        ratedialog?.show()
        ratedialog?.getWindow()?.setLayout((resources.displayMetrics.widthPixels*0.8).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
    }

    private fun showfeedbackdialog() {
        feddbackbinding = FragmentFeedbackBinding.inflate(layoutInflater)
        feedbackdialog =  AlertDialog.Builder(this)
            .setView(feddbackbinding?.root)
            .create()
            .apply {
                window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
            }

        feddbackbinding?.send?.setOnClickListener {
            if(feddbackbinding?.TextTitle?.text.isNullOrEmpty())
            {
                feddbackbinding?.TextTitle?.setError("Please enter a title")
            }
            else if(feddbackbinding?.TextDescription?.text.isNullOrEmpty())
            {
                feddbackbinding?.TextDescription?.setError("Please enter a description ")
            }
            else
            {
                var packageInfo: PackageInfo? = null
                try {
                    packageInfo = getPackageManager()?.getPackageInfo(getPackageName().toString(), 0)
                } catch (e: PackageManager.NameNotFoundException) {
                    e.printStackTrace()
                }
                val appverson = packageInfo!!.versionName
                val appname = resources.getString(R.string.app_name)
                val appversoncode = Build.VERSION.SDK_INT
                val mobilemodel = Build.MODEL
                val packagename: String = getPackageName().toString()
                PostFeedback(
                    appverson,
                    appname,
                    appversoncode.toString(),
                    mobilemodel,
                    packagename,
                    feddbackbinding?.TextTitle?.text.toString(),
                    feddbackbinding?.TextDescription?.text.toString()
                )
            }
        }

        feddbackbinding?.cancel?.setOnClickListener {
            feedbackdialog?.dismiss()
        }

        feedbackdialog?.show()

    }

    private fun PostFeedback(appverson: String?, appname: String, appversoncode: String, mobilemodel: String?, packagename: String, title: String, description: String) {

        if(!isFatching)
        {
            val dialog_ll = ProgressDialog(this)
            dialog_ll.setMessage("Send Feedback...")
            dialog_ll.setCancelable(false)
            dialog_ll.show()
            isFatching = true

            RetrofitClient.getFeedbackClient().sendfeedback(
                appname,
                packagename,
                title,
                description,
                mobilemodel,
                appversoncode,
                appverson
            )?.enqueue(object : Callback<Feedback?> {
                override fun onResponse(call: Call<Feedback?>, response: Response<Feedback?>) {
                    if (response.code() == 200) {
                        assert(response.body() != null)
                        isFatching = false
                        if (response.body()?.status!!) {
                            Toast.makeText(
                                this@SettingActivity,
                                "" + response.body()?.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            dialog_ll.dismiss()
                            feddbackbinding?.TextTitle?.text = null
                            feddbackbinding?.TextDescription?.text = null
                            feedbackdialog?.dismiss()
                        }
                    }
                }
                override fun onFailure(call: Call<Feedback?>, t: Throwable) {
                    dialog_ll.dismiss()
                    Toast.makeText(
                        this@SettingActivity,
                        "Please send feedback in playstore",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }
    }


    private fun setColorforColorPicker(hexcolorcode: String)
    {
        if(hexcolorcode == "#FFFFFF")
        {
            binding?.colorpicker?.backgroundTintList = null
        }
        else
        {
            binding?.colorpicker?.backgroundTintList = ColorStateList.valueOf(Color.parseColor(hexcolorcode))
        }
    }

    override fun colorClick(hexcolorcode: String) {

        setColorforColorPicker(hexcolorcode)
        colorDialog?.dismiss()
        CoroutineScope(Dispatchers.IO).launch {
            localDataStore.setStringInDataStore("colorforline", hexcolorcode)
            cancel()
        }
    }
}