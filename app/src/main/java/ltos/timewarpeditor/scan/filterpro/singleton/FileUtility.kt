package ltos.timewarpeditor.scan.filterpro.singleton

import android.content.Context
import android.os.Environment
import java.io.File
import java.util.*

class FileUtility(val context: Context) {

     var AppFolderName: File? = null
     var allFileList:MutableList<File>? = null

    init {
        checkFolder()
        context.cacheDir.createNewFile()
    }

    private fun checkFolder()
    {
        AppFolderName = File("${Environment.getExternalStorageDirectory()}/Download/TimeWarp")
        if(!(AppFolderName?.exists()?:false))
        {
            AppFolderName?.mkdirs()
        }
    }

    fun getOutPutFile():File{
        checkFolder()
        val nOutPutFile =  File("$AppFolderName/TimeWarp_${System.currentTimeMillis()}.jpg")
        nOutPutFile.createNewFile()
        return nOutPutFile
    }

    fun getVideoOutPutFile(): File {
        checkFolder()
        val nOutPutFile = File("$AppFolderName/TimeWarp_${System.currentTimeMillis()}.mp4")
        nOutPutFile.createNewFile()
        return nOutPutFile
    }

    fun getTempFile():File{
        val tempfile =  File(context.cacheDir, "tempFile.jpg")
        if(tempfile.exists())
        {
            tempfile.delete()
        }
        tempfile.createNewFile()
        return tempfile
    }

    fun getTempVideoFile(): File {
        val tempfile = File(context.cacheDir, "tempvideoFile.mp4")
        if (tempfile.exists()) {
            tempfile.delete()
        }
        tempfile.createNewFile()
        return tempfile
    }


    fun getAllFile():MutableList<File>
    {
        if(AppFolderName?.listFiles()!=null)
        {
            allFileList = Arrays.asList(*AppFolderName?.listFiles())
            if(allFileList != null && allFileList?.size!! > 0)
            {
                allFileList = allFileList?.filter {
                    it.name.contains("mp4") || it.name.contains("jpg")
                } as MutableList<File>?

                (allFileList as MutableList<File>).sortByDescending {
                    it.lastModified()
                }
            }
        }
        return allFileList?: mutableListOf()
    }
}