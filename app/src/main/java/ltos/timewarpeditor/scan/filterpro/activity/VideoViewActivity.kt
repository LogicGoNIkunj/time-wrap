package ltos.timewarpeditor.scan.filterpro.activity

import android.content.Intent
import android.media.MediaScannerConnection
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.MediaController
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.databinding.ActivityVideoViewBinding
import ltos.timewarpeditor.scan.filterpro.databinding.SmallNative1Binding
import ltos.timewarpeditor.scan.filterpro.singleton.FileUtility
import ltos.timewarpeditor.scan.filterpro.singleton.LocalSingelton
import ltos.timewarpeditor.scan.filterpro.utiltiy.MyApplication
import org.koin.android.ext.android.inject
import java.io.File


class VideoViewActivity : AppCompatActivity() {

    private var binding:ActivityVideoViewBinding? = null
    private val fileUtility: FileUtility by inject()
    private var fromGallery = false
    //Ad variable
    private var mInterstitialAd: InterstitialAd? = null
    private var isActivityLeft = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVideoViewBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        isActivityLeft = false

        LoadIntersstaticialAd()

        fromGallery = intent?.getBooleanExtra("fromGallery", false) == true

        setAllView()
        setAllClick()

        val mediaController = MediaController(this)
        mediaController.setAnchorView(binding?.videoView)
        binding?.videoView?.setMediaController(mediaController)

        binding?.videoView?.setVideoPath(LocalSingelton.finalVideoPath)
        binding?.videoView?.setOnPreparedListener({ mp -> mp.isLooping = true })
        binding?.videoView?.start()
    }

    private fun setAllView() {
        if(fromGallery)
        {
            binding?.savebtn?.visibility = View.GONE
            binding?.cancelbtn?.visibility = View.GONE
        }
    }

    private fun setAllClick() {
        binding?.cancelbtn?.setOnClickListener {
            onBackPressed()
        }
        binding?.savebtn?.setOnClickListener {
            saveFinalVideo()
        }
    }

    private fun saveFinalVideo() {
        val srcFile = File(LocalSingelton.finalVideoPath)
        val dstFile = fileUtility.getVideoOutPutFile()
        srcFile.copyTo(dstFile, overwrite = true)
        Toast.makeText(this, "Saved successfully", Toast.LENGTH_SHORT).show()
        LocalSingelton.finalfilepath = dstFile.path
        MediaScannerConnection.scanFile(
            this,
            arrayOf(dstFile.path),
            null
        ) { s, uri -> //
            LocalSingelton.currentUri = uri
        }
        startActivity(Intent(this, ShareActivity::class.java).putExtra("from", "video"))
        showInterstaticial()
        finish()
    }

    override fun onBackPressed() {
        if(!fromGallery)
        {
            startActivity(Intent(this, CameraActivity::class.java).putExtra("internal", "true"))
        }
        finish()
    }

    private fun LoadIntersstaticialAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.setFullScreenContentCallback(object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            super.onAdDismissedFullScreenContent()
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            super.onAdFailedToShowFullScreenContent(adError)
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            MyApplication.appOpenManager!!.isAdShow = true
                        }
                    })
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    MyApplication.appOpenManager!!.isAdShow = false
                }
            })
    }

    private fun showInterstaticial()
    {
        try
        {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd!!.show(this)
                MyApplication.appOpenManager!!.isAdShow = true
            }
        }catch (  e:java.lang.Exception)
        {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        isActivityLeft = false
    }

    override fun onResume() {
        super.onResume()
        isActivityLeft = false
    }

    override fun onPause() {
        super.onPause()
        isActivityLeft = true
    }

    override fun onStop() {
        super.onStop()
        isActivityLeft = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isActivityLeft = true
    }

}