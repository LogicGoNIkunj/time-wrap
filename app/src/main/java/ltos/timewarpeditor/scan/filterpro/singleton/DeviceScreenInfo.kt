package ltos.timewarpeditor.scan.filterpro.singleton

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager

class DeviceScreenInfo(val context: Context) {

    var dWidth = 0
    var dHeight = 0
    var dpi = 0
    init {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val displaMatrics = DisplayMetrics()
        windowManager.defaultDisplay.getRealMetrics(displaMatrics)

        dWidth = displaMatrics.widthPixels
        dHeight = displaMatrics.heightPixels
        dpi = displaMatrics.densityDpi
    }
}