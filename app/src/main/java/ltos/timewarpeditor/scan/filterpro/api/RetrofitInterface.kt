package ltos.timewarpeditor.scan.filterpro.api

import ltos.timewarpeditor.scan.filterpro.dataclass.Feedback
import ltos.timewarpeditor.scan.filterpro.dataclass.AdsModel
import ltos.timewarpeditor.scan.filterpro.dataclass.Model_Btn
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface RetrofitInterface {
    @FormUrlEncoded
    @POST("feedback")
    fun sendfeedback(
        @Field("app_name") app_name: String?,
        @Field("package_name") package_name: String?,
        @Field("title") title: String?,
        @Field("description") description: String?,
        @Field("device_name") device_name: String?,
        @Field("android_version") android_version: String?,
        @Field("version") version: String?
    ): Call<Feedback?>?

   /* @GET("applications/32/json")
    fun getAdId():Call<Data>*/

    @FormUrlEncoded
    @POST("get-ads-list")
    fun getAdId(@Field("app_id") IntVal: Int): Call<AdsModel?>?

    @GET("qureka-ad")
    fun getBtnAd(): Call<Model_Btn?>?
}