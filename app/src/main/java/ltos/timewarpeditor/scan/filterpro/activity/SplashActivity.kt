package ltos.timewarpeditor.scan.filterpro.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ThreeBounce
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import ltos.timewarpeditor.scan.filterpro.R
import ltos.timewarpeditor.scan.filterpro.api.RetrofitClient
import ltos.timewarpeditor.scan.filterpro.dataclass.AdsModel
import ltos.timewarpeditor.scan.filterpro.utiltiy.MyApplication
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SplashActivity : AppCompatActivity() {

    //Ad variable
    private var mInterstitialAd: InterstitialAd? = null
    private var isActivityLeft = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isActivityLeft = false
        MyApplication.appOpenManager!!.isAdShow = true

        setUpAnimation()
        getAdsData()
        LoadIntersstaticialAd()

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, PreCameraActivity::class.java))
            showInterstaticial()
            finish()
        }, 5000)
    }

    private fun getAdsData() {
        RetrofitClient.getAdClient().getAdId(62)!!.enqueue(object : Callback<AdsModel?> {
            override fun onResponse(call: Call<AdsModel?>, response: Response<AdsModel?>) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body()!!.isStatus()) {
                            if (response.body()!!.getData() != null) {
                                MyApplication.set_AdsInt(
                                    response.body()!!.getData().getPublisher_id()
                                )
                                if (response.body()!!.getData().getPublishers() != null) {
                                    if (MyApplication.get_AdsInt() == 1) {
                                        if (response.body()!!.getData().getPublishers()
                                                .getAdmob() != null
                                        ) {
                                            if (response.body()!!.getData().getPublishers()
                                                    .getAdmob()
                                                    .getAdmob_interstitial() != null
                                            ) {
                                                MyApplication.set_Admob_interstitial_Id(
                                                    response.body()!!.getData().getPublishers()
                                                        .getAdmob().getAdmob_interstitial()
                                                        .getAdmob_interstitial_id()
                                                )
                                            } else {
                                                MyApplication.set_Admob_interstitial_Id(
                                                    resources.getString(
                                                        R.string.interstatatial_ad_id
                                                    )
                                                )
                                            }
                                            if (response.body()!!.getData().getPublishers()
                                                    .getAdmob()
                                                    .getAdmob_native() != null
                                            ) {
                                                MyApplication.set_Admob_native_Id(
                                                    response.body()!!.getData().getPublishers()
                                                        .getAdmob().getAdmob_native()
                                                        .getAdmob_Native_id()
                                                )
                                            } else {
                                                MyApplication.set_Admob_native_Id(
                                                    resources.getString(
                                                        R.string.native_ad_id
                                                    )
                                                )
                                            }
                                            if (response.body()!!.getData().getPublishers()
                                                    .getAdmob()
                                                    .getAdmob_banner() != null
                                            ) {
                                                MyApplication.set_Admob_banner_Id(
                                                    response.body()!!.getData().getPublishers()
                                                        .getAdmob().getAdmob_banner()
                                                        .getAdmob_banner_id()
                                                )

                                            } else {
                                                MyApplication.set_Admob_banner_Id(
                                                    resources.getString(
                                                        R.string.banner_ad_id
                                                    )
                                                )
                                            }
                                            if (response.body()!!.getData().getPublishers()
                                                    .getAdmob()
                                                    .getAdmob_open() != null
                                            ) {
                                                MyApplication.set_Admob_openapp(
                                                    response.body()!!.getData().getPublishers()
                                                        .getAdmob().getAdmob_open()
                                                        .getAdmob_Open_id()
                                                )
                                            } else {
                                                MyApplication.set_Admob_openapp(
                                                    resources.getString(
                                                        R.string.open_ad_id
                                                    )
                                                )
                                            }
                                        } else {
                                            MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                                            MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                                            MyApplication.set_Admob_interstitial_Id(
                                                resources.getString(
                                                    R.string.interstatatial_ad_id
                                                )
                                            )
                                            MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                                        }
                                    } else {
                                        MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                                        MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                                        MyApplication.set_Admob_interstitial_Id(
                                            resources.getString(
                                                R.string.interstatatial_ad_id
                                            )
                                        )
                                        MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                                    }
                                } else {
                                    MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                                    MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                                    MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                                    MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                                }
                            } else {
                                MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                                MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                                MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                                MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                            }
                        } else {
                            MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                            MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                            MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                            MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                        }
                    } else {
                        MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                        MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                        MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                        MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                    }
                } else {
                    MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                    MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                    MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                    MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                }
            }

            override fun onFailure(call: Call<AdsModel?>, t: Throwable) {
                MyApplication.set_Admob_banner_Id(resources.getString(R.string.banner_ad_id))
                MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
            }
        })
    }

    private fun LoadIntersstaticialAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.setFullScreenContentCallback(object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            super.onAdDismissedFullScreenContent()
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            super.onAdFailedToShowFullScreenContent(adError)
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            MyApplication.appOpenManager!!.isAdShow = true
                        }
                    })
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    MyApplication.appOpenManager!!.isAdShow = false
                }
            })
    }

    private fun showInterstaticial()
    {
        try
        {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd!!.show(this)
                MyApplication.appOpenManager!!.isAdShow = true
            }
        }catch (  e:java.lang.Exception)
        {
            e.printStackTrace()
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setUpAnimation() {
        val relativeLayout = RelativeLayout(this)
        val progressBar = ProgressBar(this, null, 16842874)
        val doubleBounce: Sprite = ThreeBounce()
        doubleBounce.setColor(resources.getColor(R.color.black))
        progressBar.indeterminateDrawable = doubleBounce
        val layoutParams = RelativeLayout.LayoutParams(110, 110)
        layoutParams.addRule(14)
        layoutParams.addRule(12)
        layoutParams.setMargins(0, 0, 0, 50)
        relativeLayout.addView(progressBar, layoutParams)
        setContentView(relativeLayout)
    }

    override fun onStart() {
        super.onStart()
        isActivityLeft = false
    }

    override fun onResume() {
        super.onResume()
        isActivityLeft = false
    }

    override fun onPause() {
        super.onPause()
        isActivityLeft = true
    }

    override fun onStop() {
        super.onStop()
        isActivityLeft = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isActivityLeft = true
    }
}